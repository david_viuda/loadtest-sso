var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "15",
        "ok": "15",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "653",
        "ok": "653",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "179",
        "ok": "179",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "169",
        "ok": "169",
        "ko": "-"
    },
    "percentiles1": {
        "total": "123",
        "ok": "123",
        "ko": "-"
    },
    "percentiles2": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "percentiles3": {
        "total": "512",
        "ok": "512",
        "ko": "-"
    },
    "percentiles4": {
        "total": "624",
        "ok": "624",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 15,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "1.154",
        "ok": "1.154",
        "ko": "-"
    }
},
contents: {
"req_api-auth-5d5b1": {
        type: "REQUEST",
        name: "api_auth",
path: "api_auth",
pathFormatted: "req_api-auth-5d5b1",
stats: {
    "name": "api_auth",
    "numberOfRequests": {
        "total": "3",
        "ok": "3",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "653",
        "ok": "653",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "314",
        "ok": "314",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "percentiles1": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "percentiles2": {
        "total": "399",
        "ok": "399",
        "ko": "-"
    },
    "percentiles3": {
        "total": "602",
        "ok": "602",
        "ko": "-"
    },
    "percentiles4": {
        "total": "642",
        "ok": "642",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 3,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.231",
        "ok": "0.231",
        "ko": "-"
    }
}
    },"req_browse-widget-faea2": {
        type: "REQUEST",
        name: "browse_widget",
path: "browse_widget",
pathFormatted: "req_browse-widget-faea2",
stats: {
    "name": "browse_widget",
    "numberOfRequests": {
        "total": "6",
        "ok": "6",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "322",
        "ok": "322",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "101",
        "ok": "101",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "104",
        "ok": "104",
        "ko": "-"
    },
    "percentiles1": {
        "total": "57",
        "ok": "57",
        "ko": "-"
    },
    "percentiles2": {
        "total": "113",
        "ok": "113",
        "ko": "-"
    },
    "percentiles3": {
        "total": "272",
        "ok": "272",
        "ko": "-"
    },
    "percentiles4": {
        "total": "312",
        "ok": "312",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 6,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.462",
        "ok": "0.462",
        "ko": "-"
    }
}
    },"req_form-signinform-e781b": {
        type: "REQUEST",
        name: "form_signInForm",
path: "form_signInForm",
pathFormatted: "req_form-signinform-e781b",
stats: {
    "name": "form_signInForm",
    "numberOfRequests": {
        "total": "3",
        "ok": "3",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "83",
        "ok": "83",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "89",
        "ok": "89",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2",
        "ok": "2",
        "ko": "-"
    },
    "percentiles1": {
        "total": "88",
        "ok": "88",
        "ko": "-"
    },
    "percentiles2": {
        "total": "88",
        "ok": "88",
        "ko": "-"
    },
    "percentiles3": {
        "total": "88",
        "ok": "88",
        "ko": "-"
    },
    "percentiles4": {
        "total": "88",
        "ok": "88",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 3,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.231",
        "ok": "0.231",
        "ko": "-"
    }
}
    },"req_login-d56b6": {
        type: "REQUEST",
        name: "login",
path: "login",
pathFormatted: "req_login-d56b6",
stats: {
    "name": "login",
    "numberOfRequests": {
        "total": "3",
        "ok": "3",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "189",
        "ok": "189",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "453",
        "ok": "453",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "291",
        "ok": "291",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "115",
        "ok": "115",
        "ko": "-"
    },
    "percentiles1": {
        "total": "231",
        "ok": "231",
        "ko": "-"
    },
    "percentiles2": {
        "total": "342",
        "ok": "342",
        "ko": "-"
    },
    "percentiles3": {
        "total": "430",
        "ok": "430",
        "ko": "-"
    },
    "percentiles4": {
        "total": "448",
        "ok": "448",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 3,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.231",
        "ok": "0.231",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
