package com.mobgen.performance.sso.simulation

import com.mobgen.performance.sso.conf.SimulationConfig
import com.mobgen.performance.sso.request.SsoMiddleware
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation


/**
  * Created by david.viuda on 01/09/16.
  */
// TODO: finish and test, users_to_register.csv
class SsoRegistrationFlow extends Simulation with SimulationConfig {

  val usersToRegister = csv("users_to_register.csv")

  val ssoRegistration = scenario("SSO Registration Flow")
    .feed(usersToRegister)
    .repeat(1) {
      exec(
      SsoMiddleware.apiAuth(),
      doPause(),
      SsoMiddleware.getRegistrationForm(),
      doPause(),
      SsoMiddleware.traditionalRegistration()
    )
  }

  val ssoBaseUrl = getRequiredString("sso_base_url")

  setUp(
    ssoRegistration
      .inject(atOnceUsers(1))
      .protocols(SsoMiddleware.getHttpConf(ssoBaseUrl))
  )

}
