package com.mobgen.performance.sso.simulation

import com.mobgen.performance.sso.conf.SimulationConfig
import com.mobgen.performance.sso.request.SsoMiddleware
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation

/**
  * Created by david.viuda on 14/09/16.
  */
// TODO: finish, check whether possible to migrate deleted sso account. Cannot be done due to email activation
class SsoMigrationFlow extends Simulation with SimulationConfig {

  val usersToMigrate = csv("users_to_migrate.csv")

  val ssoMigration = scenario("SSO Migration Flow")
    // set up also api_token here as "" by default
    // set up also accept_version here, take from config
    .exec(session =>
          session.set("digest", getRequiredString("app.sso.digest"))
      //            .set("acceptVersion", getRequiredInt("app.sso.accept_version"))
      //            .set("apiToken", getRequiredString("")) // default value "
                  .set("ssoBaseWidgetUrl", getEnvRequiredString("sso_widget_base_url"))
  )
    .feed(usersToMigrate)
    //.during(1200) {
    .repeat(1) {
    exec(
      SsoMiddleware.apiAuth(),
      doPause(),
      SsoMiddleware.browseWidgedMigration(),
      doPause(),
      SsoMiddleware.getMigrationForm(),
      doPause(),
      SsoMiddleware.migrateAccount(),
      doPause(),
      //SsoMiddleware.
      doPause(),
      SsoMiddleware.traditionalLogin(),
      doPause(),
      SsoMiddleware.deleteAccount(),
      doPause()
    )
  }

  val ssoBaseUrl = getEnvRequiredString("sso_base_url")

  setUp(
    ssoMigration
      .inject(atOnceUsers(3))
      .protocols(SsoMiddleware.getHttpConf(ssoBaseUrl))
  )

}
