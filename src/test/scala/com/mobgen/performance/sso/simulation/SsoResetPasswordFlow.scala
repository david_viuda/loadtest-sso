package com.mobgen.performance.sso.simulation

import com.mobgen.performance.sso.conf.SimulationConfig
import com.mobgen.performance.sso.request.SsoMiddleware
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation

/**
  * Created by david.viuda on 07/09/16.
  */
class SsoResetPasswordFlow extends Simulation with SimulationConfig {

  val usersToResetPassword = csv("users_to_login.csv")

  val ssoResetPassword = scenario("SSO Reset Password Flow")
    // set up also api_token here as "" by default
    // set up also accept_version here, take from config
    .exec(session =>
      session.set("digest", getRequiredString("app.sso.digest"))
    )
    .feed(usersToResetPassword)
    //.during(1200) {
    .repeat(1) {
      exec(
        SsoMiddleware.apiAuth(),
        doPause(),
        SsoMiddleware.getSignInForm(),
        doPause(),
        SsoMiddleware.traditionalLogin(),
        doPause()
      )
  }

  val ssoBaseUrl = getEnvRequiredString("sso_base_url")

  setUp(
    ssoResetPassword
      .inject(atOnceUsers(3))
      .protocols(SsoMiddleware.getHttpConf(ssoBaseUrl))
  )

}
