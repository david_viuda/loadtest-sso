package com.mobgen.performance.sso.simulation

import com.mobgen.performance.sso.conf.SimulationConfig
import com.mobgen.performance.sso.request.SsoMiddleware
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation

/**
  * Created by david.viuda on 01/09/16.
  */
// TODO: add getTranslations
class SsoLoginFlow extends Simulation with SimulationConfig {

  val usersToLogin = csv("users_to_login.csv")

  val ssoLogin = scenario("SSO Login Flow")
    // set up also api_token here as "" by default
    // set up also accept_version here, take from config
    .exec(session =>
          session.set("digest", getEnvRequiredString("sso_digest"))
//            .set("acceptVersion", getRequiredInt("app.sso.accept_version"))
//            .set("apiToken", getRequiredString("")) // default value "
                  .set("ssoBaseWidgetUrl", getEnvRequiredString("sso_widget_base_url"))
    )
    .feed(usersToLogin)
    //.during(1200) {
    .repeat(1) {
      exec(
        SsoMiddleware.apiAuth(),
        doPause(),
        SsoMiddleware.browseWidgetLandingPage(),
        doPause(),
        SsoMiddleware.getSignInForm(),
        doPause(),
        SsoMiddleware.browseWidgetLoginPage(),
        doPause(),
        SsoMiddleware.traditionalLogin(),
        doPause()
      )
    }

  val ssoBaseUrl = getEnvRequiredString("sso_base_url")

  setUp(
    ssoLogin
      .inject(atOnceUsers(3))
      .protocols(SsoMiddleware.getHttpConf(ssoBaseUrl))
  )

}
