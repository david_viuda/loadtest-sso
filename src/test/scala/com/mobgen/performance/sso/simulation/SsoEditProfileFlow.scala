package com.mobgen.performance.sso.simulation

import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import com.mobgen.performance.sso.conf.SimulationConfig
import com.mobgen.performance.sso.request.SsoMiddleware


/**
  * Created by david.viuda on 13/09/16.
  */
// TODO: finish
class SsoEditProfileFlow extends Simulation with SimulationConfig {

  val usersToLogin = csv("users_to_login.csv")

  val ssoProfile = scenario("SSO Profile Flow")
    // set up also api_token here as "" by default
    // set up also accept_version here, take from config
    .exec(session =>
            session.set("digest", getRequiredString("app.sso.digest"))
      //            .set("acceptVersion", getRequiredInt("app.sso.accept_version"))
      //            .set("apiToken", getRequiredString("")) // default value "
            .set("ssoBaseWidgetUrl", getEnvRequiredString("sso_widget_base_url"))
    )
    .feed(usersToLogin)
    //.during(1200) {
    .repeat(1) {
      exec(
        SsoMiddleware.apiAuth(),
        doPause(),
        SsoMiddleware.traditionalLogin(),
        doPause(),
        SsoMiddleware.browseWidgetProfile(),
        SsoMiddleware.getTranslations(),
        doPause(),
        SsoMiddleware.updateUserProfile(),
        doPause()
      )
    }

  val ssoBaseUrl = getEnvRequiredString("sso_base_url")

  setUp(
    ssoProfile
      .inject(atOnceUsers(3))
      .protocols(SsoMiddleware.getHttpConf(ssoBaseUrl))
  )

}
