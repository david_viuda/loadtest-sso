package com.mobgen.performance.sso.request

import com.mobgen.performance.sso.conf.Headers
import io.gatling.core.Predef._
import io.gatling.http.Predef._

/**
  * Created by david.viuda on 31/08/16.
  */
object SsoMiddleware {

  /**
    * Object declaring the resources available in SSO middleware
    * apiAuth needed to retrieve the token used in the rest of requests
    */

  // Fields from JSON responses
  val API_TOKEN_KEY: String = "$.api_token"
  val RESPONSE_CODE_STATUS_KEY: String = "$.code"
  val ACCESS_TOKEN_KEY: String = "$.accessToken"
  val TRADITIONAL_REGISTRATION_STAT_KEY: String = "$.stat"
  val PROFILE_UID_KEY: String = "$.uid"
  val CHECK_EMAIL_STAT_KEY: String = "$.stat"
  val RESET_PASSWORD_TOKEN_STAT_KEY = "$.stat"
  val MIGRATE_ACCOUNT_STAT_KEY = "$.stat"
  val TRANSLATIONS_KEY = "$.translations"

  // Fields stored in session
  //val AUTH_TOKEN_SAVED: String = "api_auth"
  val API_TOKEN_SAVED: String = "api_token"
  val API_TOKEN_RESPONSE_SAVED: String = "api_token_json"
  val UUID_SAVED: String = "uuid"

  // Widget Endpoints
  val WIDGET_LANDING_PAGE_POSTFIX: String = "/"
  val WIDGET_LOGIN_LANDING_PAGE_POSTFIX: String = "/login"
  val WIDGET_PROFILE_LANDING_PAGE_POSTFIX: String = "/profile"
  val WIDGET_MIGRATION_LANDING_PAGE_POSTFIX: String = "/migration"
  // Middleware Endpoints
  val AUTHENTICATION_POSTFIX: String = "/api/sso/api_auth"
  val SIGNIN_FORM_POSTFIX: String = "/api/form/signInForm"
  val REGISTRATION_FORM_POSTFIX: String = "/api/form/registrationForm"
  val MIGRATION_FORM_POSTFIX: String = "/api/form/migrationForm"
  val EDIT_PROFILE_FORM_POSTFIX:String = "/api/form/" // check endpoint
  val EMAIL_REGISTRATION_POSTFIX: String = "/api/sso/raas"
  val LOGIN_POSTFIX: String = "/api/sso/login"
  val REFRESH_TOKEN_POSTFIX: String = "/api/sso/refresh"
  val GET_PROFILE_POSTFIX: String = "/api/sso/profile"
  val UPDATE_PROFILE_POSTFIX: String = "/api/sso/user_profile"
  val TRANSLATIONS_POSTFIX: String = "/api/sso/translations"
  val SETTINGS_CONTENT_POSTFIX: String = "/api/sso/settingsContent"
  val CHECK_EMAIL_POSTFIX: String = "/api/sso/check_email"
  val RESET_PASSWORD_TOKEN_POSTFIX: String = "/api/sso/reset_password_token"
  val MIGRATE_ACCOUNT_POSTFIX: String = "/api/sso/migrateAccount"
  val DELETE_ACCOUNT_POSTFIX: String = "/api/sso/account"

  // Request params
  val CONTENT_CACHE_PARAM_KEY: String = "cache"
  val CONTENT_MARKET_PARAM_KEY: String = "market"
  val CONTENT_VERSION_PARAM_KEY: String = "version"

  // Retrieves API token for SSO services and saves it to session
  def apiAuth() = {
    exec(
      http("api_auth")
        .post(AUTHENTICATION_POSTFIX)
        .body(ElFileBody("SsoApiAuth.txt")).asJSON
        .check(jsonPath(API_TOKEN_KEY).saveAs(API_TOKEN_SAVED))
    )
  }

  // Browse landing widget page
  def browseWidgetLandingPage() = {
    exec(
      http("browse_widget")
        .get("${ssoBaseWidgetUrl}" + WIDGET_LANDING_PAGE_POSTFIX)
    )
  }

  // Browse landing widget page for login
  def browseWidgetLoginPage() = {
    exec(
      http("browse_widget_login")
        .get("${ssoBaseWidgetUrl}" + WIDGET_LOGIN_LANDING_PAGE_POSTFIX)
    )
  }

  // Browse landing widget page for profile
  // TODO: test
  def browseWidgetProfile() = {
    exec(
      http("browse_widget_profile")
        .get("${ssoBaseWidgetUrl}" + WIDGET_PROFILE_LANDING_PAGE_POSTFIX)
    )
  }

  // TODO: test
  def browseWidgedMigration() = {
    exec(
      http("browse_widget_migration")
        .get("${ssoBaseWidgetUrl}" + WIDGET_MIGRATION_LANDING_PAGE_POSTFIX)
    )
  }

  // Retrieves Sign in form for traditional login
  def getSignInForm() = {
    exec(
      http("form_signInForm")
        .get(SIGNIN_FORM_POSTFIX)
        .header("Authorization", "Basic ${api_token}")
    )
  }

  // Retrieves form for traditional registration
  // TODO: test
  def getRegistrationForm() = {
    exec(
      http("form_registrationForm")
        .get(REGISTRATION_FORM_POSTFIX)
    )
  }

  // Retrieves form for migration
  // TODO: test
  def getMigrationForm() = {
    exec(
      http("form_migrationForm")
        .get(MIGRATION_FORM_POSTFIX)
    )
  }

  // Performs traditional registration
  // TODO: finish and test
  def traditionalRegistration() = {
    exec(
      http("raas")
        .post(EMAIL_REGISTRATION_POSTFIX)
        .body(ElFileBody("SsoEmailRegistration.txt")).asJSON
        .check(jsonPath(TRADITIONAL_REGISTRATION_STAT_KEY).is("ok"))
    )
  }

  // Performs traditional login
  // TODO: add check, test uuid in session
  def traditionalLogin() = {
    exec(
      http("login")
        .post(LOGIN_POSTFIX)
        .header("Authorization", "Basic ${api_token}")
        .body(ElFileBody("SsoEmailLogin.txt")).asJSON
        //.check(jsonPath(ACCESS_TOKEN_KEY)) // Checks whether login is successful
        .check(jsonPath(PROFILE_UID_KEY).saveAs(UUID_SAVED))
    )
  }

  // Refreshes access token
  // TODO: test and create scenario
  def refreshToken() = {
    exec(
      http("refresh")
        .post(REFRESH_TOKEN_POSTFIX)
        .body(ElFileBody("SsoRefreshToken.txt"))
        .check(jsonPath(ACCESS_TOKEN_KEY))
    )
  }

  // Retrieves user profile
  // TODO: test
  def getUserProfile() = {
    exec(
      http("profile")
        .get(GET_PROFILE_POSTFIX)
        .check(jsonPath(PROFILE_UID_KEY))
    )
  }

  // Updates user profile
  // TODO: review implementation
  def updateUserProfile() = {
    exec(
      http("profile")
        .put(UPDATE_PROFILE_POSTFIX)
        .body(ElFileBody("SsoUpdateProfile.txt")).asJSON
        .check(jsonPath(ACCESS_TOKEN_KEY))
    )
  }

  // Retrieves translations for the web widget
  // TODO: test
  def getTranslations() = {
    exec(
      http("translations")
        .get(TRANSLATIONS_POSTFIX)
        .header("Authorization", "Basic ${api_token}")
        .queryParamMap(Map(
          CONTENT_CACHE_PARAM_KEY -> "false",
          CONTENT_MARKET_PARAM_KEY -> "en-ZA",
          CONTENT_VERSION_PARAM_KEY -> "1.0.0"
        ))
        .check(jsonPath(TRANSLATIONS_KEY))
    )
  }

  // Retrieves the content needed to populate the settings pages in the widget
  // TODO: test
  def getSettingsContent() = {
    exec(
      http("settings_content")
        .get(SETTINGS_CONTENT_POSTFIX)
        .header("Authorization", "Basic ${api_token}")
        .queryParamMap(Map(
          CONTENT_CACHE_PARAM_KEY -> "false",
          CONTENT_MARKET_PARAM_KEY -> "en-ZA",
          CONTENT_VERSION_PARAM_KEY -> "1.0.0"
        ))
        //.check(jsonPath(SETTINGS_CONTENT_KEY))
    )
  }

  // TODO: review implementation
  def checkEmail() = {
    exec(
      http("check_email")
        .post(CHECK_EMAIL_POSTFIX)
        .header("Authorization", "Basic ${api_token}")
        .body(ElFileBody("SsoEmailCheck.txt")).asJSON
        .check(jsonPath(CHECK_EMAIL_STAT_KEY).is("ok"))
    )
  }

  // TODO: review implementation
  def resetPasswordToken() = {
    exec(
      http("reset_password_token")
        .post(RESET_PASSWORD_TOKEN_POSTFIX)
        .header("Authorization", "Basic ${api_token}")
        .body(ElFileBody("")).asJSON
        .check(jsonPath(RESET_PASSWORD_TOKEN_STAT_KEY).is("ok"))
    )
  }

  // TODO: add check, do implementation
  def migrateAccount() = {
    exec(
      http("migrate_account")
        .post(MIGRATE_ACCOUNT_POSTFIX)
        .header("Authorization", "Basic ${api_token}")
        .body(ElFileBody("SsoMigrateAccount.txt.txt")).asJSON
        .check(jsonPath(MIGRATE_ACCOUNT_STAT_KEY).is("ok"))
        //.check(jsonPath(PROFILE_UID_KEY).saveAs(UUID_SAVED))
    )
  }

  // TODO: review params, endpoint
  def deleteAccount() = {
    exec(
      http("delete_account")
        .delete(DELETE_ACCOUNT_POSTFIX)
        .header("Authorization", "Basic ${api_token}")
        //.formParam("uuid","${uuid}")
        .body(ElFileBody("SsoDeleteAccount.txt.txt")).asJSON
    )
  }

  def activateAccount() = {
    exec(
      http("activate_account")
        .get("")
    )
  }

//  def resetPassword() = {
//    exec(
//      http("reset_password")
//        .post(RESET_PASSWORD_POSTFIX)
//        .header("Authorization", "Basic ${api_token}")
//        .body(ElFileBody("")).asJSON
//        .check(jsonPath())
//    )
//  }


  def getHttpConf(url: String) = {
    http
      .baseURL(url) //set up url in session instead of here?
      .headers(Headers.ssoHeaders)
  }

}
