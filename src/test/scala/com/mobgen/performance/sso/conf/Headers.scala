package com.mobgen.performance.sso.conf

import io.gatling.http.{HeaderNames, HeaderValues}

/**
  * Created by david.viuda on 31/08/16.
  */
object Headers extends SimulationConfig {

  val HEADER_SSO_ACCEPT_VERSION: String = "accept-version"

  // perhaps do in Simulation?
  val acceptVersion = getRequiredString("app.sso.accept_version")

  val ssoHeaders = Map(
    HeaderNames.ContentType -> HeaderValues.ApplicationJson,
    //HeaderNames.Authorization -> "Basic ${api_token}",
    HEADER_SSO_ACCEPT_VERSION -> acceptVersion

  )
}
