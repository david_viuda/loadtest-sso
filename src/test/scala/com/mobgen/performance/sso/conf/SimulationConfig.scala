package com.mobgen.performance.sso.conf

import akka.event.slf4j.SLF4JLogging

import com.typesafe.config.{Config, ConfigException, ConfigFactory}
import io.gatling.core.Predef._

import scala.util.Try

/**
  * Created by david.viuda on 30/08/16.
  */
trait SimulationConfig extends SLF4JLogging {

  /**
    * Application config object.
    */
  private[this] val config = ConfigFactory.load()

  val environmentConfigSettings: Config = config.getConfig("env.qa");


  /**
    * Gets the required string from the config file or throws
    * an exception if the string is not found.
    *
    * @param path path to string
    * @return string fetched by path
    */

  def getRequiredString(path: String) = {
    Try(config.getString(path)).getOrElse {
      handleError(path)
    }
  }

  def getEnvRequiredString(path: String) = {
    Try(environmentConfigSettings.getString(path)).getOrElse {
      handleError(path)
    }
  }

  /**
    * Gets the required int from the config file or throws
    * an exception if the int is not found.
    *
    * @param path path to int
    * @return int fetched by path
    */
  def getRequiredInt(path: String) = {
    Try(config.getInt(path)).getOrElse {
      handleError(path)
    }
  }

  private[this] def handleError(path: String) = {
    val errMsg = s"Missing required configuration entry in application.conf : $path"
    log.error(errMsg)
    throw new ConfigException.Missing(errMsg)

  }

  def doPause() = pause(1, getRequiredInt("data.think_time"))

  val pauseTime = getRequiredInt("data.think_time")

}
